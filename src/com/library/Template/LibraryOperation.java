package com.library.Template;

public abstract class LibraryOperation {
    public final void performOperation() {
        validate();
        process();
        update();
    }

    protected abstract void validate();
    protected abstract void process();
    protected abstract void update();
}

class LoginOperation extends LibraryOperation {
    @Override
    protected void validate() {
        // Validate login credentials
    }

    @Override
    protected void process() {
        // Process login
    }

    @Override
    protected void update() {
        // Update login status
    }
}

class NewStudentEntry extends LibraryOperation {
    @Override
    protected void validate() {
        // Validate new student details
    }

    @Override
    protected void process() {
        // Process new student entry
    }

    @Override
    protected void update() {
        // Update student records
    }
}

class NewBookEntry extends LibraryOperation {
    @Override
    protected void validate() {
        // Validate new book details
    }

    @Override
    protected void process() {
        // Process new book entry
    }

    @Override
    protected void update() {
        // Update book records
    }
}

class IssueBook extends LibraryOperation {
    @Override
    protected void validate() {
        // Validate book issue details
    }

    @Override
    protected void process() {
        // Process book issue
    }

    @Override
    protected void update() {
        // Update issue records
    }
}

class ReturnBook extends LibraryOperation {
    @Override
    protected void validate() {
        // Validate book return details
    }

    @Override
    protected void process() {
        // Process book return
    }

    @Override
    protected void update() {
        // Update return records
    }
}

class Records extends LibraryOperation {
    @Override
    protected void validate() {
        // Validate records request
    }

    @Override
    protected void process() {
        // Process records request
    }

    @Override
    protected void update() {
        // Update displayed records
    }
}