package com.library.Template;

class LoginOperation extends LibraryOperation {
    @Override
    protected void validate() {
        // Validate login credentials
        System.out.println("Validating login credentials.");
    }

    @Override
    protected void process() {
        // Process login
        System.out.println("Processing login.");
    }

    @Override
    protected void update() {
        // Update login status
        System.out.println("Updating login status.");
    }
}

class NewStudentEntry extends LibraryOperation {
    @Override
    protected void validate() {
        // Validate new student details
        System.out.println("Validating new student details.");
    }

    @Override
    protected void process() {
        // Process new student entry
        System.out.println("Processing new student entry.");
    }

    @Override
    protected void update() {
        // Update student records
        System.out.println("Updating student records.");
    }
}

class NewBookEntry extends LibraryOperation {
    @Override
    protected void validate() {
        // Validate new book details
        System.out.println("Validating new book details.");
    }

    @Override
    protected void process() {
        // Process new book entry
        System.out.println("Processing new book entry.");
    }

    @Override
    protected void update() {
        // Update book records
        System.out.println("Updating book records.");
    }
}

class IssueBook extends LibraryOperation {
    @Override
    protected void validate() {
        // Validate book issue details
        System.out.println("Validating book issue details.");
    }

    @Override
    protected void process() {
        // Process book issue
        System.out.println("Processing book issue.");
    }

    @Override
    protected void update() {
        // Update issue records
        System.out.println("Updating issue records.");
    }
}

class ReturnBook extends LibraryOperation {
    @Override
    protected void validate() {
        // Validate book return details
        System.out.println("Validating book return details.");
    }

    @Override
    protected void process() {
        // Process book return
        System.out.println("Processing book return.");
    }

    @Override
    protected void update() {
        // Update return records
        System.out.println("Updating return records.");
    }
}

class Records extends LibraryOperation {
    @Override
    protected void validate() {
        // Validate records request
        System.out.println("Validating records request.");
    }

    @Override
    protected void process() {
        // Process records request
        System.out.println("Processing records request.");
    }

    @Override
    protected void update() {
        // Update displayed records
        System.out.println("Updating displayed records.");
    }
}
