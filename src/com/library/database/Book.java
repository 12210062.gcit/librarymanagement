// src/com/library/database/Book.java
package com.library.database;

public class Book {
    private int bookId;
    private String name;
    private int publisherYear;
    private double price;
    private String authorName;

    // Constructor
    public Book(int bookId, String name, int publisherYear, double price, String authorName) {
        this.bookId = bookId;
        this.name = name;
        this.publisherYear = publisherYear;
        this.price = price;
        this.authorName = authorName;
    }

    // Getters and Setters
    public int getBookId() {
        return bookId;
    }

    public void setBookId(int bookId) {
        this.bookId = bookId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPublisherYear() {
        return publisherYear;
    }

    public void setPublisherYear(int publisherYear) {
        this.publisherYear = publisherYear;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getAuthorName() {
        return authorName;
    }

    public void setAuthorName(String authorName) {
        this.authorName = authorName;
    }

    // toString method for displaying book information
    @Override
    public String toString() {
        return "Book{" +
               "bookId=" + bookId +
               ", name='" + name + '\'' +
               ", publisherYear=" + publisherYear +
               ", price=" + price +
               ", authorName='" + authorName + '\'' +
               '}';
    }
}
