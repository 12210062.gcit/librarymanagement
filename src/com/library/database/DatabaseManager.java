package com.library.database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.sql.Date;

// Framework
public class DatabaseManager {
    private static DatabaseManager instance;
    private Connection connection;
    private String url = "jdbc:mysql://localhost:4306/LibraryManagementSystem";
    private String username = "root";
    private String password = ""; 

    private DatabaseManager() throws SQLException {
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            this.connection = DriverManager.getConnection(url, username, password);
        } catch (ClassNotFoundException e) {
            System.err.println("MySQL JDBC Driver not found. Include it in your library path.");
            e.printStackTrace();
            throw new SQLException(e);
        }
    }

    public static DatabaseManager getInstance() throws SQLException {
        if (instance == null) {
            instance = new DatabaseManager();
        } else if (instance.getConnection().isClosed()) {
            instance = new DatabaseManager();
        }
        return instance;
    }

    public Connection getConnection() {
        return connection;
    }

    // Implementations
    public boolean bookExists(int bookId) {
        String query = "SELECT COUNT(*) FROM books WHERE book_id = ?";
        try (PreparedStatement preparedStatement = connection.prepareStatement(query)) {
            preparedStatement.setInt(1, bookId);
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                if (resultSet.next()) {
                    int count = resultSet.getInt(1);
                    return count > 0;
                }
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return false;
    }
    
    public void addStudent(int studentID, String name, String department, int year) throws SQLException {
        String sql = "INSERT INTO students (student_id, name, department, year) VALUES (?, ?, ?, ?)";
        try (PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setInt(1, studentID);
            statement.setString(2, name);
            statement.setString(3, department);
            statement.setInt(4, year);
            statement.executeUpdate();
        }
    }

    public void addBook(int bookId, String name, int publisherYear, double price, String authorName) {
        if (bookExists(bookId)) {
            System.out.println("Book with ID " + bookId + " already exists.");
            return;
        }

        String query = "INSERT INTO books (book_id, name, publisher_year, price, author_name) VALUES (?, ?, ?, ?, ?)";
        try (PreparedStatement preparedStatement = connection.prepareStatement(query)) {
            preparedStatement.setInt(1, bookId);
            preparedStatement.setString(2, name);
            preparedStatement.setInt(3, publisherYear);
            preparedStatement.setDouble(4, price);
            preparedStatement.setString(5, authorName);
            preparedStatement.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    public void issueBook(int bookId, int studentId, Date issueDate, Date dueDate) {
        String query = "INSERT INTO issued_books (book_id, student_id, issue_date, due_date) VALUES (?, ?, ?, ?)";
        try (PreparedStatement preparedStatement = connection.prepareStatement(query)) {
            preparedStatement.setInt(1, bookId);
            preparedStatement.setInt(2, studentId);
            preparedStatement.setDate(3, issueDate);
            preparedStatement.setDate(4, dueDate);
            preparedStatement.executeUpdate();
            System.out.println("Book issued successfully!");
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }
    
    public void returnBook(int bookId, int studentId, Date returnDate) throws SQLException {
        String query = "INSERT INTO returned_books (book_id, student_id, return_date) VALUES (?, ?, ?)";
        try (PreparedStatement preparedStatement = connection.prepareStatement(query)) {
            preparedStatement.setInt(1, bookId);
            preparedStatement.setInt(2, studentId);
            preparedStatement.setDate(3, returnDate);
            preparedStatement.executeUpdate();
            System.out.println("Book returned successfully!");
        }
    } 

    // Method to fetch all students
    public List<Student> getAllStudents() {
        List<Student> students = new ArrayList<>();
        String query = "SELECT student_id, name, department, year FROM students";
        try (PreparedStatement preparedStatement = connection.prepareStatement(query);
             ResultSet resultSet = preparedStatement.executeQuery()) {

            while (resultSet.next()) {
                int student_id = resultSet.getInt("student_id");
                String name = resultSet.getString("name");
                String department = resultSet.getString("department");
                int year = resultSet.getInt("year");
                students.add(new Student(student_id, name, department, year));
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return students;
    }
    
    // Method to get the count of students in the database
    public int getStudentCount() {
        int count = 0;
        String query = "SELECT COUNT(*) FROM students";
        try (PreparedStatement preparedStatement = connection.prepareStatement(query);
             ResultSet resultSet = preparedStatement.executeQuery()) {
            if (resultSet.next()) {
                count = resultSet.getInt(1);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return count;
    }

    public List<Book> getBooks() {
        return fetchBooksFromDatabase();
    }

    private List<Book> fetchBooksFromDatabase() {
        List<Book> books = new ArrayList<>();
        String query = "SELECT * FROM books";
        try (PreparedStatement preparedStatement = connection.prepareStatement(query);
             ResultSet resultSet = preparedStatement.executeQuery()) {

            while (resultSet.next()) {
                int bookId = resultSet.getInt("book_id");
                String name = resultSet.getString("name");
                int publisherYear = resultSet.getInt("publisher_year");
                double price = resultSet.getDouble("price");
                String authorName = resultSet.getString("author_name");
                books.add(new Book(bookId, name, publisherYear, price, authorName));
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return books;
    }

    public List<Book> searchBooksByAuthor(String authorName) {
        List<Book> books = new ArrayList<>();
        String query = "SELECT * FROM books WHERE author_name LIKE ?";
        try (PreparedStatement preparedStatement = connection.prepareStatement(query)) {
            preparedStatement.setString(1, "%" + authorName + "%");
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                while (resultSet.next()) {
                    int bookId = resultSet.getInt("book_id");
                    String name = resultSet.getString("name");
                    int publisherYear = resultSet.getInt("publisher_year");
                    double price = resultSet.getDouble("price");
                    String author = resultSet.getString("author_name");
                    books.add(new Book(bookId, name, publisherYear, price, author));
                }
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return books;
    }

    // Method to fetch issued books from the database
    public List<IssuedBook> getIssuedBooks() {
        List<IssuedBook> issuedBooks = new ArrayList<>();
        String query = "SELECT * FROM issued_books";
        try (PreparedStatement preparedStatement = connection.prepareStatement(query);
             ResultSet resultSet = preparedStatement.executeQuery()) {
            while (resultSet.next()) {
                int id = resultSet.getInt("id");
                int bookId = resultSet.getInt("book_id");
                int studentId = resultSet.getInt("student_id");
                Date issueDate = resultSet.getDate("issue_date");
                Date dueDate = resultSet.getDate("due_date");
                issuedBooks.add(new IssuedBook(id, bookId, studentId, issueDate, dueDate));
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return issuedBooks;
    }

     // Method to fetch returned books from the database
     public List<ReturnedBook> getReturnedBooks() {
        List<ReturnedBook> returnedBooks = new ArrayList<>();
        String query = "SELECT * FROM returned_books";
        try (PreparedStatement preparedStatement = connection.prepareStatement(query);
             ResultSet resultSet = preparedStatement.executeQuery()) {
            while (resultSet.next()) {
                int id = resultSet.getInt("id");
                int bookId = resultSet.getInt("book_id");
                int studentId = resultSet.getInt("student_id");
                Date returnDate = resultSet.getDate("return_date");
                returnedBooks.add(new ReturnedBook(id, bookId, studentId, returnDate));
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return returnedBooks;
    }
    
    // Add the deleteStudent method
    public void deleteStudent(int studentID) throws SQLException {
        String sql = "DELETE FROM students WHERE student_id = ?";
        try (PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setInt(1, studentID);
            statement.executeUpdate();
        }
    }
}
