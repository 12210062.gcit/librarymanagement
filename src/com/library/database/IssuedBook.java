package com.library.database;

import java.util.Date;

public class IssuedBook {
    private int id;
    private int bookId;
    private int studentId;
    private Date issueDate;
    private Date dueDate;

    public IssuedBook(int id, int bookId, int studentId, Date issueDate, Date dueDate) {
        this.id = id;
        this.bookId = bookId;
        this.studentId = studentId;
        this.issueDate = issueDate;
        this.dueDate = dueDate;
    }

    public int getId() { return id; }
    public int getBookId() { return bookId; }
    public int getStudentId() { return studentId; }
    public Date getIssueDate() { return issueDate; }
    public Date getDueDate() { return dueDate; }
}
