package com.library.database;

import java.util.Date;

public class ReturnedBook {
    private int id;
    private int bookId;
    private int studentId;
    private Date returnDate;

    public ReturnedBook(int id, int bookId, int studentId, Date returnDate) {
        this.id = id;
        this.bookId = bookId;
        this.studentId = studentId;
        this.returnDate = returnDate;
    }

    public int getId() { return id; }
    public int getBookId() { return bookId; }
    public int getStudentId() { return studentId; }
    public Date getReturnDate() { return returnDate; }
}
