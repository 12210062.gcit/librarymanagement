package com.library.database;

public class Student {
    private int studentID;
    private String name;
    private String department;
    private int year;

    // Constructor
    public Student(int studentID, String name, String department, int year) {
        this.studentID = studentID;
        this.name = name;
        this.department = department;
        this.year = year;
    }

    // Getter for studentID
    public int getStudentID() {
        return studentID;
    }

    // Getter for name
    public String getName() {
        return name;
    }

    // Getter for department
    public String getDepartment() {
        return department;
    }

    // Getter for year
    public int getYear() {
        return year;
    }
}
