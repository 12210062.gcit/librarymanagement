package com.library.factory;

public class FormFactory {
    public static Form createForm(String type) {
        switch (type) {
            case "NewStudent":
                return new NewStudentForm();
            case "NewBookEntry":
                return new NewBookForm();
            case "IssueBook":
                return new IssueBookForm();
            case "ReturnBook":
                return new ReturnBookForm();
            default:
                throw new IllegalArgumentException("Unknown form type");
        }
    }
}
