package com.library.factory;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import com.library.database.DatabaseManager;
import java.text.SimpleDateFormat;
import java.text.ParseException;


public class IssueBookForm extends Form {
    @Override
    public void display() {
        JFrame frame = new JFrame("Issued Books Form");
        frame.setSize(400, 400);
        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        frame.setLocationRelativeTo(null);

        JPanel panel = new JPanel();
        frame.add(panel);
        placeComponents(panel);

        frame.setVisible(true);
    }

    private void placeComponents(JPanel panel) {
        panel.setLayout(null);

        // Label and Text Field for Book ID
        JLabel bookIdLabel = new JLabel("Book ID:");
        bookIdLabel.setBounds(10, 20, 80, 25);
        panel.add(bookIdLabel);

        JTextField bookIdText = new JTextField(20);
        bookIdText.setBounds(150, 20, 165, 25);
        panel.add(bookIdText);

        // Label and Text Field for Student ID
        JLabel studentIdLabel = new JLabel("Student ID:");
        studentIdLabel.setBounds(10, 60, 80, 25);
        panel.add(studentIdLabel);

        JTextField studentIdText = new JTextField(20);
        studentIdText.setBounds(150, 60, 165, 25);
        panel.add(studentIdText);

        // Label and Text Field for Issue Date
        JLabel issueDateLabel = new JLabel("Issue Date:");
        issueDateLabel.setBounds(10, 100, 80, 25);
        panel.add(issueDateLabel);

        JTextField issueDateText = new JTextField(20);
        issueDateText.setBounds(150, 100, 165, 25);
        panel.add(issueDateText);

        // Label and Text Field for Due Date
        JLabel dueDateLabel = new JLabel("Due Date:");
        dueDateLabel.setBounds(10, 140, 80, 25);
        panel.add(dueDateLabel);

        JTextField dueDateText = new JTextField(20);
        dueDateText.setBounds(150, 140, 165, 25);
        panel.add(dueDateText);

        // Submit Button
        JButton submitButton = new JButton("Submit");
        submitButton.setBounds(10, 180, 100, 25);
        panel.add(submitButton);

        // Submit Button ActionListener
        submitButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    int bookId = Integer.parseInt(bookIdText.getText());
                    int studentId = Integer.parseInt(studentIdText.getText());
                    String issueDateString = issueDateText.getText();
                    String dueDateString = dueDateText.getText();

                    // Perform validation
                    if (issueDateString.isEmpty() || dueDateString.isEmpty()) {
                        JOptionPane.showMessageDialog(null, "Issue Date and Due Date cannot be empty.");
                        return;
                    }

                    // Convert String dates to java.sql.Date objects
                    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
                    java.util.Date parsedIssueDate = dateFormat.parse(issueDateString);
                    java.util.Date parsedDueDate = dateFormat.parse(dueDateString);
                    java.sql.Date issueDate = new java.sql.Date(parsedIssueDate.getTime());
                    java.sql.Date dueDate = new java.sql.Date(parsedDueDate.getTime());

                    // Call method to issue book to the student
                    try {
                        DatabaseManager.getInstance().issueBook(bookId, studentId, issueDate, dueDate);
                        JOptionPane.showMessageDialog(null, "Book issued successfully!");
                        ((JFrame) SwingUtilities.getWindowAncestor(panel)).dispose();
                    } catch (SQLException ex) {
                        ex.printStackTrace();
                        JOptionPane.showMessageDialog(null, "Error issuing book.");
                    }
                } catch (NumberFormatException ex) {
                    JOptionPane.showMessageDialog(null, "Please enter valid numeric values for Book ID and Student ID.");
                } catch (ParseException ex) {
                    JOptionPane.showMessageDialog(null, "Please enter dates in the format yyyy-MM-dd.");
                }
            }
        });
    }
}
