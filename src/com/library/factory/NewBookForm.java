package com.library.factory;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import com.library.database.DatabaseManager;

public class NewBookForm extends Form {
    @Override
    public void display() {
        JFrame frame = new JFrame("New Book Form");
        frame.setSize(400, 400);
        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        frame.setLocationRelativeTo(null);

        JPanel panel = new JPanel();
        frame.add(panel);
        placeComponents(panel);

        frame.setVisible(true);
    }

    private void placeComponents(JPanel panel) {
        panel.setLayout(null);

        // Label and Text Field for Book ID
        JLabel bookIdLabel = new JLabel("Book ID:");
        bookIdLabel.setBounds(10, 20, 80, 25);
        panel.add(bookIdLabel);

        JTextField bookIdText = new JTextField(20);
        bookIdText.setBounds(150, 20, 165, 25);
        panel.add(bookIdText);

        // Label and Text Field for Name
        JLabel nameLabel = new JLabel("Name:");
        nameLabel.setBounds(10, 60, 80, 25);
        panel.add(nameLabel);

        JTextField nameText = new JTextField(20);
        nameText.setBounds(150, 60, 165, 25);
        panel.add(nameText);

        // Label and Text Field for Publisher Year
        JLabel publisherYearLabel = new JLabel("Publisher Year:");
        publisherYearLabel.setBounds(10, 100, 100, 25);
        panel.add(publisherYearLabel);

        JTextField publisherYearText = new JTextField(20);
        publisherYearText.setBounds(150, 100, 165, 25);
        panel.add(publisherYearText);

        // Label and Text Field for Price
        JLabel priceLabel = new JLabel("Price:");
        priceLabel.setBounds(10, 140, 80, 25);
        panel.add(priceLabel);

        JTextField priceText = new JTextField(20);
        priceText.setBounds(150, 140, 165, 25);
        panel.add(priceText);

        // Label and Text Field for Author Name
        JLabel authorNameLabel = new JLabel("Author Name:");
        authorNameLabel.setBounds(10, 180, 100, 25);
        panel.add(authorNameLabel);

        JTextField authorNameText = new JTextField(20);
        authorNameText.setBounds(150, 180, 165, 25);
        panel.add(authorNameText);

        // Submit Button
        JButton submitButton = new JButton("Submit");
        submitButton.setBounds(10, 220, 100, 25);
        panel.add(submitButton);

        // Submit Button ActionListener
        submitButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    int bookId = Integer.parseInt(bookIdText.getText());
                    String name = nameText.getText();
                    String publisherYearInput = publisherYearText.getText();
                    double price = Double.parseDouble(priceText.getText());
                    String authorName = authorNameText.getText();

                    // Perform validation
                    if (name.isEmpty() || authorName.isEmpty()) {
                        JOptionPane.showMessageDialog(null, "Name and Author Name cannot be empty.");
                        return;
                    }

                    // Validate Publisher Year input
                    if (publisherYearInput.length() != 4 || !publisherYearInput.matches("\\d{4}")) {
                        JOptionPane.showMessageDialog(null, "Publisher Year must be a 4-digit number.");
                        return;
                    }

                    int publisherYear = Integer.parseInt(publisherYearInput);

                    // Check if book ID already exists
                    try {
                        if (DatabaseManager.getInstance().bookExists(bookId)) {
                            JOptionPane.showMessageDialog(null, "Book with ID " + bookId + " already exists.");
                            return;
                        }
                    } catch (SQLException ex) {
                        ex.printStackTrace();
                        JOptionPane.showMessageDialog(null, "Error checking book existence in the database.");
                        return;
                    }

                    // Call method to insert book into the database
                    try {
                        DatabaseManager.getInstance().addBook(bookId, name, publisherYear, price, authorName);
                        JOptionPane.showMessageDialog(null, "Book added successfully!");
                        ((JFrame) SwingUtilities.getWindowAncestor(panel)).dispose();
                    } catch (SQLException ex) {
                        ex.printStackTrace();
                        JOptionPane.showMessageDialog(null, "Error adding book to the database.");
                    }
                } catch (NumberFormatException ex) {
                    JOptionPane.showMessageDialog(null, "Please enter valid numeric values for Book ID, Publisher Year, and Price.");
                }
            }
        });
    }
}
