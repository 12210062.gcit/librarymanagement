package com.library.factory;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import com.library.database.DatabaseManager;

public class NewStudentForm extends Form {
    @Override
    public void display() {
        JFrame frame = new JFrame("New Student Form");
        frame.setSize(400, 300);
        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        frame.setLocationRelativeTo(null);

        JPanel panel = new JPanel();
        frame.add(panel);
        placeComponents(panel);

        frame.setVisible(true);
    }

    private void placeComponents(JPanel panel) {
        panel.setLayout(null);

        // Label and Text Field for Student ID
        JLabel idLabel = new JLabel("Student ID:");
        idLabel.setBounds(10, 20, 80, 25);
        panel.add(idLabel);

        JTextField idText = new JTextField(20);
        idText.setBounds(150, 20, 165, 25);
        panel.add(idText);

        // Label and Text Field for Name
        JLabel nameLabel = new JLabel("Name:");
        nameLabel.setBounds(10, 60, 80, 25);
        panel.add(nameLabel);

        JTextField nameText = new JTextField(20);
        nameText.setBounds(150, 60, 165, 25);
        panel.add(nameText);

        // Label and ComboBox for Department
        JLabel departmentLabel = new JLabel("Department:");
        departmentLabel.setBounds(10, 100, 80, 25);
        panel.add(departmentLabel);

        String[] departments = { "AI", "BC", "FS" };
        JComboBox<String> departmentComboBox = new JComboBox<>(departments);
        departmentComboBox.setBounds(150, 100, 165, 25);
        panel.add(departmentComboBox);

        // Label and Text Field for Year
        JLabel yearLabel = new JLabel("Year:");
        yearLabel.setBounds(10, 140, 80, 25);
        panel.add(yearLabel);

        JTextField yearText = new JTextField(20);
        yearText.setBounds(150, 140, 165, 25);
        panel.add(yearText);

        // Submit Button
        JButton submitButton = new JButton("Submit");
        submitButton.setBounds(10, 180, 100, 25);
        panel.add(submitButton);

        // Submit Button ActionListener
        submitButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String studentIDText = idText.getText();
                String name = nameText.getText();
                String department = (String) departmentComboBox.getSelectedItem();
                String yearTextStr = yearText.getText();

                // Perform validation
                if (studentIDText.isEmpty() || name.isEmpty() || yearTextStr.isEmpty()) {
                    JOptionPane.showMessageDialog(null, "Student ID, Name, and Year cannot be empty.");
                    return;
                }

                int studentID;
                int year;
                try {
                    studentID = Integer.parseInt(studentIDText);
                } catch (NumberFormatException ex) {
                    JOptionPane.showMessageDialog(null, "Student ID must be a number.");
                    return;
                }

                try {
                    year = Integer.parseInt(yearTextStr);
                } catch (NumberFormatException ex) {
                    JOptionPane.showMessageDialog(null, "Year must be a number.");
                    return;
                }

                try {
                    DatabaseManager.getInstance().addStudent(studentID, name, department, year);
                    JOptionPane.showMessageDialog(null, "Student added successfully!");
                    ((JFrame) SwingUtilities.getWindowAncestor(panel)).dispose();
                } catch (SQLException ex) {
                    ex.printStackTrace();
                    JOptionPane.showMessageDialog(null, "Student ID Already Exists");
                }
            }
        });
    }
}
