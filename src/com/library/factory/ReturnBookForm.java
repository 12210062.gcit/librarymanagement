

package com.library.factory;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import com.library.database.DatabaseManager;
import java.text.SimpleDateFormat;
import java.text.ParseException;

public class ReturnBookForm extends Form {
    @Override
    public void display() {
        JFrame frame = new JFrame("Return Books Form");
        frame.setSize(400, 400);
        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        frame.setLocationRelativeTo(null);

        JPanel panel = new JPanel();
        frame.add(panel);
        placeComponents(panel);

        frame.setVisible(true);
    }

    private void placeComponents(JPanel panel) {
        panel.setLayout(null);

        // Label and Text Field for Book ID
        JLabel bookIdLabel = new JLabel("Book ID:");
        bookIdLabel.setBounds(10, 20, 80, 25);
        panel.add(bookIdLabel);

        JTextField bookIdText = new JTextField(20);
        bookIdText.setBounds(150, 20, 165, 25);
        panel.add(bookIdText);

        // Label and Text Field for Student ID
        JLabel studentIdLabel = new JLabel("Student ID:");
        studentIdLabel.setBounds(10, 60, 80, 25);
        panel.add(studentIdLabel);

        JTextField studentIdText = new JTextField(20);
        studentIdText.setBounds(150, 60, 165, 25);
        panel.add(studentIdText);

        // Label and Text Field for Return Date
        JLabel returnDateLabel = new JLabel("Return Date:");
        returnDateLabel.setBounds(10, 100, 80, 25);
        panel.add(returnDateLabel);

        JTextField returnDateText = new JTextField(20);
        returnDateText.setBounds(150, 100, 165, 25);
        panel.add(returnDateText);

        // Submit Button
        JButton submitButton = new JButton("Submit");
        submitButton.setBounds(10, 140, 100, 25);
        panel.add(submitButton);

        // Submit Button ActionListener
        submitButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    int bookId = Integer.parseInt(bookIdText.getText());
                    int studentId = Integer.parseInt(studentIdText.getText());
                    String returnDateString = returnDateText.getText();

                    // Perform validation
                    if (returnDateString.isEmpty()) {
                        JOptionPane.showMessageDialog(null, "Return Date cannot be empty.");
                        return;
                    }

                    // Convert String date to java.sql.Date object
                    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
                    java.util.Date parsedReturnDate = dateFormat.parse(returnDateString);
                    java.sql.Date returnDate = new java.sql.Date(parsedReturnDate.getTime());

                    // Call method to return book
                    try {
                        DatabaseManager.getInstance().returnBook(bookId, studentId, returnDate);
                        JOptionPane.showMessageDialog(null, "Book returned successfully!");
                        ((JFrame) SwingUtilities.getWindowAncestor(panel)).dispose();
                    } catch (SQLException ex) {
                        ex.printStackTrace();
                        JOptionPane.showMessageDialog(null, "Error returning book.");
                    }
                } catch (NumberFormatException ex) {
                    JOptionPane.showMessageDialog(null, "Please enter valid numeric values for Book ID and Student ID.");
                } catch (ParseException ex) {
                    JOptionPane.showMessageDialog(null, "Please enter dates in the format yyyy-MM-dd.");
                }
            }
        });
    }
}
