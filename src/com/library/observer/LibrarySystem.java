package com.library.observer;

import com.library.database.DatabaseManager;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class LibrarySystem implements Subject {
    private List<Observer> observers = new ArrayList<>();

    @Override
    public void registerObserver(Observer observer) {
        observers.add(observer);
    }

    @Override
    public void removeObserver(Observer observer) {
        observers.remove(observer);
    }

    @Override
    public void notifyObservers() {
        for (Observer observer : observers) {
            observer.update();
        }
    }

    // Call this method when there's a change that observers need to know about
    public void changeOccurred() {
        notifyObservers();
    }

    // Method to fetch the number of students from the database
    public int getNumberOfStudents() {
        try {
            DatabaseManager dbManager = DatabaseManager.getInstance();
            return dbManager.getStudentCount();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }

    // Define a method to display records
    public void displayRecords() {
        int numberOfStudents = getNumberOfStudents();
        System.out.println("Number of students: " + numberOfStudents);
    }
}
