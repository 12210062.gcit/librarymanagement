package com.library.search;

import com.library.database.Book;
import com.library.database.DatabaseManager;
import java.util.List;
import java.util.stream.Collectors;

public class SearchByAuthor implements SearchStrategy {
    private DatabaseManager dbManager;

    public SearchByAuthor(DatabaseManager dbManager) {
        this.dbManager = dbManager;
    }

    @Override
    public List<Book> search(String authorName) {
        return dbManager.getBooks().stream()
                .filter(book -> book.getAuthorName().equalsIgnoreCase(authorName))
                .collect(Collectors.toList());
    }
}
