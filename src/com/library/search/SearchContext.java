
//Strategy Design Pattern is used to implement different search strategies for searching books in a library database.



package com.library.search;

import com.library.database.Book;
import java.util.List;

public class SearchContext {
    private SearchStrategy strategy;

    public void setStrategy(SearchStrategy strategy) {
        this.strategy = strategy;
    }

    public List<Book> executeSearch(String query) {
        return strategy.search(query);
    }
}
