package com.library.search;

import com.library.database.Book;
import java.util.List;

public interface SearchStrategy {
    List<Book> search(String query);
}
