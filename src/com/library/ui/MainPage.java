
package com.library.ui;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import com.library.factory.Form;
import com.library.factory.FormFactory;
import com.library.observer.LibrarySystem;
import com.library.database.DatabaseManager;
import com.library.database.Student;
import com.library.database.Book;
import com.library.database.IssuedBook;
import com.library.database.ReturnedBook;
import java.sql.SQLException;
import java.util.List;

public class MainPage extends JFrame {
    private JButton newStudentButton;
    private JButton newBookButton;
    private JButton issueBookButton;
    private JButton returnBookButton;
    private JButton recordButton;
    private JButton searchButton;
    private JTextField searchField;
    private JButton logoutButton;
    private LibrarySystem librarySystem;

    public MainPage(LibrarySystem librarySystem) {
        this.librarySystem = librarySystem;
        setTitle("Library Management System");
        setSize(400, 350);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLocationRelativeTo(null);

        JPanel panel = new JPanel();
        add(panel);
        placeComponents(panel);

        newStudentButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Form form = FormFactory.createForm("NewStudent");
                form.display();
            }
        });

        newBookButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Form form = FormFactory.createForm("NewBookEntry");
                form.display();
            }
        });

        issueBookButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Form form = FormFactory.createForm("IssueBook");
                form.display();
            }
        });

        returnBookButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Form form = FormFactory.createForm("ReturnBook");
                form.display();
            }
        });

        recordButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    DatabaseManager dbManager = DatabaseManager.getInstance();
                    List<Book> books = dbManager.getBooks();
                    List<Student> students = dbManager.getAllStudents();
                    List<IssuedBook> issuedBooks = dbManager.getIssuedBooks();
                    List<ReturnedBook> returnedBooks = dbManager.getReturnedBooks(); // Fetch ReturnedBook data
                    RecordsTable recordsTable = new RecordsTable(books, students, issuedBooks, returnedBooks); // Pass all four lists
                    recordsTable.setVisible(true);
                } catch (SQLException ex) {
                    ex.printStackTrace();
                    JOptionPane.showMessageDialog(MainPage.this, "Error fetching records.");
                }
            }
        });

        searchButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String authorName = searchField.getText().trim();
                if (!authorName.isEmpty()) {
                    try {
                        DatabaseManager dbManager = DatabaseManager.getInstance();
                        List<Book> books = dbManager.searchBooksByAuthor(authorName);
                        List<Student> students = dbManager.getAllStudents();
                        List<IssuedBook> issuedBooks = dbManager.getIssuedBooks();
                        List<ReturnedBook> returnedBooks = dbManager.getReturnedBooks(); // Fetch ReturnedBook data for consistent display
                        RecordsTable recordsTable = new RecordsTable(books, students, issuedBooks, returnedBooks);
                        recordsTable.setVisible(true);
                    } catch (SQLException ex) {
                        ex.printStackTrace();
                        JOptionPane.showMessageDialog(MainPage.this, "Error searching books by author.");
                    }
                } else {
                    JOptionPane.showMessageDialog(MainPage.this, "Please enter an author name to search.");
                }
            }
        });

        logoutButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                LoginPage loginPage = new LoginPage(librarySystem);
                loginPage.setVisible(true);
                dispose();
            }
        });
    }

    private void placeComponents(JPanel panel) {
        panel.setLayout(null);

        newStudentButton = new JButton("New Student");
        newStudentButton.setBounds(10, 20, 150, 25);
        panel.add(newStudentButton);

        newBookButton = new JButton("New Book");
        newBookButton.setBounds(10, 50, 150, 25);
        panel.add(newBookButton);

        issueBookButton = new JButton("Issue Book");
        issueBookButton.setBounds(10, 80, 150, 25);
        panel.add(issueBookButton);

        returnBookButton = new JButton("Return Book");
        returnBookButton.setBounds(10, 110, 150, 25);
        panel.add(returnBookButton);

        recordButton = new JButton("Records");
        recordButton.setBounds(10, 140, 150, 25);
        panel.add(recordButton);

        searchField = new JTextField();
        searchField.setBounds(10, 200, 150, 25);
        panel.add(searchField);

        searchButton = new JButton("Search by Author");
        searchButton.setBounds(170, 200, 180, 25);
        panel.add(searchButton);

        logoutButton = new JButton("Logout");
        logoutButton.setBounds(10, 170, 150, 25);
        panel.add(logoutButton);
    }
}
