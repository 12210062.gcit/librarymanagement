package com.library.ui;

import com.library.database.Book;
import com.library.database.Student;
import com.library.database.IssuedBook; // Import the IssuedBook class
import com.library.database.ReturnedBook; // Import the ReturnedBook class
import com.library.database.DatabaseManager; // Import DatabaseManager class

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.util.List;

public class RecordsTable extends JFrame {
    public RecordsTable(List<Book> books, List<Student> students, List<IssuedBook> issuedBooks, List<ReturnedBook> returnedBooks) {
        setTitle("Records");
        setSize(1000, 800); // Adjusted size to accommodate four tables
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        setLocationRelativeTo(null);

        JPanel panel = new JPanel(new GridLayout(4, 1)); // Use GridLayout for four tables
        add(panel);

        // Book records table
        JPanel bookPanel = new JPanel(new BorderLayout());
        JLabel bookTitle = new JLabel("Books", SwingConstants.CENTER);
        bookPanel.add(bookTitle, BorderLayout.NORTH);
        
        String[] bookColumnNames = {"Book ID", "Name", "Publisher Year", "Price", "Author Name"};
        DefaultTableModel bookTableModel = new DefaultTableModel(bookColumnNames, 0);
        for (Book book : books) {
            Object[] row = {book.getBookId(), book.getName(), book.getPublisherYear(), book.getPrice(), book.getAuthorName()};
            bookTableModel.addRow(row);
        }
        JTable bookTable = new JTable(bookTableModel);
        JScrollPane bookScrollPane = new JScrollPane(bookTable);
        bookPanel.add(bookScrollPane, BorderLayout.CENTER);
        panel.add(bookPanel);

        // Student records table
        JPanel studentPanel = new JPanel(new BorderLayout());
        JLabel studentTitle = new JLabel("Students", SwingConstants.CENTER);
        studentPanel.add(studentTitle, BorderLayout.NORTH);
        
        String[] studentColumnNames = {"Student ID", "Name", "Department", "Year"};
        DefaultTableModel studentTableModel = new DefaultTableModel(studentColumnNames, 0);
        for (Student student : students) {
            Object[] row = {student.getStudentID(), student.getName(), student.getDepartment(), student.getYear()};
            studentTableModel.addRow(row);
        }
        JTable studentTable = new JTable(studentTableModel);
        JScrollPane studentScrollPane = new JScrollPane(studentTable);
        studentPanel.add(studentScrollPane, BorderLayout.CENTER);
        
        // Adding delete button and action for student table
        JButton deleteButton = new JButton("Delete Selected Student");
        deleteButton.setFont(new Font("Arial", Font.BOLD, 14)); // Set font
        deleteButton.setBackground(Color.RED); // Set background color
        deleteButton.setForeground(Color.WHITE); // Set foreground color
        deleteButton.setBorder(BorderFactory.createLineBorder(Color.BLACK, 2)); // Set border
        studentPanel.add(deleteButton, BorderLayout.SOUTH);
        
        deleteButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int selectedRow = studentTable.getSelectedRow();
                if (selectedRow >= 0) {
                    int studentID = (int) studentTableModel.getValueAt(selectedRow, 0);
                    try {
                        DatabaseManager.getInstance().deleteStudent(studentID);
                        studentTableModel.removeRow(selectedRow);
                        JOptionPane.showMessageDialog(null, "Student deleted successfully!");
                    } catch (SQLException ex) {
                        ex.printStackTrace();
                        JOptionPane.showMessageDialog(null, "Error deleting student.");
                    }
                } else {
                    JOptionPane.showMessageDialog(null, "Please select a student to delete.");
                }
            }
        });

        panel.add(studentPanel);

        // Issued books table
        JPanel issuedBookPanel = new JPanel(new BorderLayout());
        JLabel issuedBookTitle = new JLabel("Issued Books", SwingConstants.CENTER);
        issuedBookPanel.add(issuedBookTitle, BorderLayout.NORTH);

        String[] issuedBookColumnNames = {"ID", "Book ID", "Student ID", "Issue Date", "Due Date"};
        DefaultTableModel issuedBookTableModel = new DefaultTableModel(issuedBookColumnNames, 0);
        for (IssuedBook issuedBook : issuedBooks) {
            Object[] row = {issuedBook.getId(), issuedBook.getBookId(), issuedBook.getStudentId(), issuedBook.getIssueDate(), issuedBook.getDueDate()};
            issuedBookTableModel.addRow(row);
        }
        JTable issuedBookTable = new JTable(issuedBookTableModel);
        JScrollPane issuedBookScrollPane = new JScrollPane(issuedBookTable);
        issuedBookPanel.add(issuedBookScrollPane, BorderLayout.CENTER);
        panel.add(issuedBookPanel);

        // Returned books table
        JPanel returnedBookPanel = new JPanel(new BorderLayout());
        JLabel returnedBookTitle = new JLabel("Returned Books", SwingConstants.CENTER);
        returnedBookPanel.add(returnedBookTitle, BorderLayout.NORTH);

        String[] returnedBookColumnNames = {"ID", "Book ID", "Student ID", "Return Date"};
        DefaultTableModel returnedBookTableModel = new DefaultTableModel(returnedBookColumnNames, 0);
        for (ReturnedBook returnedBook : returnedBooks) {
            Object[] row = {returnedBook.getId(), returnedBook.getBookId(), returnedBook.getStudentId(), returnedBook.getReturnDate()};
            returnedBookTableModel.addRow(row);
        }
        JTable returnedBookTable = new JTable(returnedBookTableModel);
        JScrollPane returnedBookScrollPane = new JScrollPane(returnedBookTable);
        returnedBookPanel.add(returnedBookScrollPane, BorderLayout.CENTER);
        panel.add(returnedBookPanel);
    }
}
